# all the imports
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash




from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__) # create the application instance :)
app.config.from_object(__name__) # load config from this file , flaskr.py
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///flaskr.db'
# Load default config and override config from an environment variable
app.config.update(dict(
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

db = SQLAlchemy(app)
db.init_app(app)

from flaskr import models
with app.app_context():
    db.create_all()

#used for populating selectfield of categories
def getCategories():
    with app.app_context():
        x = [(a.id, a.name) for a in models.Category.query.all()]
        return x


#imports all the routes for app
from flaskr.views.routes import flaskr_blueprint
app.register_blueprint(flaskr_blueprint)

#Adds to an empty db if not it was just created
with app.app_context():
    if not models.User.query.filter(models.User.id == 1).all():
        db.session.add(models.User(name='Admin', username=app.config['USERNAME'], pw=app.config['PASSWORD']))
        db.session.add(models.Category(id=1, name='tech'))
        db.session.add(models.Category(id=2, name='other'))
        db.session.commit()







from flaskr.views import api
app.register_blueprint(api.user_blueprint)
app.register_blueprint(api.post_blueprint)
app.register_blueprint(api.category_blueprint)