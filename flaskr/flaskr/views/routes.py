from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, Blueprint

from flaskr.flaskr import db, getCategories
from flaskr import models
from flaskr import forms

flaskr_blueprint = Blueprint('flaskr', __name__, url_prefix='')
#Viewing Functions
#shows the entries in the database based on id number
#     -id number is based on time of entry(unless specified); newest at top
@flaskr_blueprint.route('/')
def show_entries():
    entries = models.Post.query.all()
    return render_template('show_entries.html', entries=entries)


#adds entries with POST requests and redirects back to show_entries main page otherwise fails with error
@flaskr_blueprint.route('/add', methods=['GET', 'POST'])
def add():
    if not session.get('logged_in'):
        abort(401)
    form = forms.Add(request.form)
    form.category_id.choices = getCategories()
    error = 0
    if request.method == 'POST' and form.validate():
        db.session.add(models.Post(title=form.title.data,
                                   text=form.text.data,
                                   author=request.cookies.get('YourSessionCookie'),
                                   category_id=form.category_id.data))
        db.session.commit()
        flash('Added Succesfully')
        return redirect(url_for('flaskr.show_entries'))
    return render_template('add.html', error=error, form=form)


#removes entries with POST requests and redirects back to show_entries main page otherwise fails with error
@flaskr_blueprint.route('/remove', methods=['GET', 'POST'])
def remove():
    if not session.get('logged_in'):
        abort(401)
    form = forms.Remove(request.form)
    error = 0
    if request.method == 'POST' and form.validate():
        if models.Post.query.filter(models.Post.title == form.title.data).all():
            post = models.Post.query.filter_by(title=form.title.data).one()
            if post.author == request.cookies.get('YourSessionCookie') or \
                            request.cookies.get('YourSessionCookie') == 'admin':
                db.session.delete(post)
                db.session.commit()
                flash('Removed Succesfully')
                return redirect(url_for('flaskr.show_entries'))

            else:
                error = 'Must be admin or author to remove posts'
        else:
            error = 'Invalid Title'
    return render_template('remove.html', error=error, form=form)


# login and out functions
#login
@flaskr_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    error = 0
    form = forms.LoginForm(request.form)
    if request.method == 'POST' and form.validate():

        #check if user is in user db
        if models.User.query.filter(models.User.username == form.username.data).all():

            #check if user pw is correct
            if form.password.data == models.User.query.filter_by(username=form.username.data).one().pw:
                user = models.User.query.filter_by(username=form.username.data).one()
                session['logged_in'] = True
                flash('You are now logged in')
                response = redirect(url_for('flaskr.show_entries'))
                response.set_cookie('YourSessionCookie', user.username)
                return response

            else:
                error = 'Invalid Password'

        else:
            error = 'Invalid Username'

    return render_template('login.html', error=error, form=form)




#logout
@flaskr_blueprint.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('flaskr.show_entries'))



#adds user with POST requests and redirects back to show_entries main page otherwise fails with error
@flaskr_blueprint.route('/adduser', methods=['GET', 'POST'])
def adduser():
    form = forms.Adduser(request.form)
    error = 0
    if request.method == 'POST':
        if form.validate():
            db.session.add(models.User(name=form.name.data, username=form.username.data, pw=form.pw.data))
            db.session.commit()
            flash('Added Succesfully')
            session['logged_in'] = True
            response = redirect(url_for('flaskr.show_entries'))
            user = models.User.query.filter_by(username=form.username.data).one()
            response.set_cookie('YourSessionCookie', user.username)
            return response
        else:
            error = 'All field lengths must be between 1 and 25 characters.'
    return render_template('adduser.html', error=error, form=form)


#adds user with POST requests and redirects back to show_entries main page otherwise fails with error
@flaskr_blueprint.route('/removeuser', methods=['GET', 'POST'])
def removeuser():
    form = forms.Removeuser(request.form)
    error = 0
    if request.method == 'POST':
        if form.validate():
            if models.User.query.filter(models.User.username == form.username.data).all():
                user = models.User.query.filter_by(username=form.username.data).one()
                if user.username == request.cookies.get('YourSessionCookie') or \
                                request.cookies.get('YourSessionCookie') == 'admin':
                    db.session.delete(user)
                    db.session.commit()
                    flash('Removed Succesfully')

                    if request.cookies.get('YourSessionCookie') != 'admin':
                        return redirect(url_for('logout'))
                    return redirect(url_for('flaskr.show_entries'))

                else:
                    error = 'Must be admin to remove other users'
            else:
                error = 'Invalid Username'
    return render_template('removeuser.html', error=error, form=form)


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()

@flaskr_blueprint.route('/shutdown', methods=['GET', 'POST'])
def shutdown():
    session.clear()
    shutdown_server()
    return 'Server shutting down...'

@flaskr_blueprint.route('/addcategory', methods=['GET', 'POST'])
def addcategory():
    if not session.get('logged_in'):
        abort(401)
    form = forms.AddCategory(request.form)
    if request.method == 'POST' and form.validate():
        db.session.add(models.Category(name=form.name.data))
        db.session.commit()
        flash('Added Succesfully')
        return redirect(url_for('flaskr.show_entries'))
    return render_template('addcategory.html', error=0, form=form)
