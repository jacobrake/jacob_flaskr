import flask_restless
# Create the Flask-Restless API manager.
from flaskr.flaskr import db, app
from flaskr import models
with app.app_context():
    manager = flask_restless.APIManager(app, flask_sqlalchemy_db=db)
    # Create API endpoints, which will be available at /api/<tablename> by
    # default. Allowed HTTP methods can be specified as well.
    user_blueprint = manager.create_api_blueprint(models.User, methods=['GET', 'POST', 'DELETE', 'PATCH'])
    post_blueprint = manager.create_api_blueprint(models.Post,
                                                  methods=['GET', 'POST', 'DELETE', 'PATCH'],
                                                  exclude_columns=['category_id'],
                                                  include_methods=['category'])
    category_blueprint = manager.create_api_blueprint(models.Category, methods=['GET'])




