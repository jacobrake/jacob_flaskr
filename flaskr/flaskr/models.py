from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from flaskr.flaskr import db


class User(db.Model):
    __tablename__ = 'users'
    __table_args__ = {'extend_existing': True}
    id = db.Column(Integer, primary_key=True)
    name = db.Column(String(20))
    username = db.Column(String(20))
    pw = db.Column(String(20))
    posts = db.relationship('Post')


class Post(db.Model):
    __tablename__ = 'entries'
    id = db.Column(Integer, primary_key=True)
    title = db.Column(String(50))
    text = db.Column(String(300))
    category_id = db.Column(Integer, ForeignKey('categories.id'))
    author = db.Column(String, ForeignKey('users.username'))

    def category(self):
        return Category.query.filter_by(id=self.category_id).one().name


class Category(db.Model):
    __tablename__ = 'categories'
    id = db.Column(Integer, primary_key=True)
    name = db.Column(String(50))
    posts = db.relationship('Post')