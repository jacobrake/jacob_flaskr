from wtforms import Form, BooleanField, StringField, PasswordField, validators, SelectField
from wtforms.widgets import TextArea

from flaskr.flaskr import getCategories
class LoginForm(Form):
    username = StringField('Username', [validators.Length(min=1, max=25)])
    password = StringField('Password', [validators.Length(min=1, max=25)])

class Add(Form):
    title = StringField('Title', [validators.Length(max=25)], default='Title')
    text = StringField('Text', [validators.Length(max=400)], widget=TextArea())
    category_id = SelectField('Category', choices=getCategories(), coerce=int)
    
class Remove(Form):
    title = StringField('Title', [validators.Length(max=25)])

class Adduser(Form):
    name = StringField('Name', [validators.Length(min=1, max=25)])
    username = StringField('Username', [validators.Length(min=1, max=25)])
    pw = StringField('Password', [validators.Length(min=1, max=25)])

class Removeuser(Form):
    username = StringField('Username', [validators.Length(min=1, max=25)])


class AddCategory(Form):
    name = StringField('Name', [validators.Length(max=25)])